import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationHelper {
  static Future<void> configFirebaseMessaging() async {
    FirebaseMessaging messaging = FirebaseMessaging.instance;
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();
    AndroidNotificationChannel channel = AndroidNotificationChannel(
      'high_importance_channel', // id
      'High Importance Notifications', // title
      'This channel is used for important notifications.', // description
      importance: Importance.high,
    );

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
    var initializationSettingsAndroid =
        AndroidInitializationSettings("@drawable/appicon");
    var initializationSettings =
        InitializationSettings(android: initializationSettingsAndroid);
    flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onSelectNotification: (value) async {},
    );

    messaging.getInitialMessage().then((value) {});

    FirebaseMessaging.onBackgroundMessage(
      (message) async {
        print('A new onBackgroundMessage event was published!');
        print('\n \n message data : ${message.data} \n');
      },
    );

    FirebaseMessaging.onMessage.listen(
      (RemoteMessage? message) {
        RemoteNotification? notification = message!.notification!;
        AndroidNotification? android = message.notification!.android!;
        AppleNotification? ios = message.notification!.apple!;
        if (notification != null) {
          flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,

                // TODO add a proper drawable resource to android, for now using
                icon: "appicon",
                //      one that already
                priority: Priority.high,
                channelShowBadge: true,
              ),
            ),
            payload: message.data['deeplink'],
          );
          print("\n \n message resived on Foreground \n \n");
          print(
              "messageTitle: ${message.notification!.title}...messageBody: ${message.notification!.body}");
        }
      },
    );

    FirebaseMessaging.onMessageOpenedApp.listen(
      (RemoteMessage message) async {
        print('\n\n\n\n\n\n A new onMessageOpenedApp event was published!');
      },
    );
  }
}
