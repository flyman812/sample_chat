import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:sample_chat/providers/providerInit.dart';
import 'package:sample_chat/services/NotificationHelper.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await NotificationHelper.configFirebaseMessaging();
  runApp(ProviderInit());
}
