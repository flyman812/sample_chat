import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hive/hive.dart';

part 'user.g.dart';

@HiveType(typeId: 0)
class UserModel {
  UserModel({
    this.id,
    this.name,
    this.email,
    this.photoUrl,
  });

  factory UserModel.fromMap(DocumentSnapshot doc) {
    return UserModel(
      id: doc.id,
      name: doc.get("name"),
      email: doc.get("email"),
      photoUrl: doc.get("photoURL"),
    );
  }

  @HiveField(0)
  final String? id;
  @HiveField(1)
  final String? name;
  @HiveField(2)
  final String? email;
  @HiveField(3)
  final String? photoUrl;
}
