import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hive/hive.dart';

part 'Mesage.g.dart';


@HiveType(typeId: 2)
class Message {
  Message({this.id, this.content, this.idFrom, this.idTo, this.timestamp});

  factory Message.fromFireStore(DocumentSnapshot doc) {
    final Map<String, dynamic> data = doc.data as Map<String, dynamic>;

    return Message(
        id: doc.id,
        content: data['content'],
        idFrom: data['idFrom'],
        idTo: data['idTo'],
        timestamp: data['timestamp']);
  }

  @HiveField(0)
  String? id;
  @HiveField(1)
  String? content;
  @HiveField(2)
  String? idFrom;
  @HiveField(3)
  String? idTo;
  @HiveField(4)
  DateTime? timestamp;
}
