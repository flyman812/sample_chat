import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hive/hive.dart';


part 'convo.g.dart';

@HiveType(typeId: 1)
class Convo {
  Convo({this.id, this.userIds, this.lastMessage});

  factory Convo.fromFireStore(DocumentSnapshot doc) {

    return Convo(
        id: doc.id,
        userIds: doc.get("users") ?? <dynamic>[],
        lastMessage: doc.get("lastMessage") ?? <dynamic>{});
  }

  @HiveField(0)
  String? id;
  @HiveField(1)
  List<dynamic>? userIds;
  @HiveField(2)
  Map<dynamic, dynamic>? lastMessage;
}


