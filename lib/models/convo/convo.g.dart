// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'convo.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class ConvoAdapter extends TypeAdapter<Convo> {
  @override
  final int typeId = 1;

  @override
  Convo read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Convo(
      id: fields[0] as String?,
      userIds: (fields[1] as List?)?.cast<dynamic>(),
      lastMessage: (fields[2] as Map?)?.cast<dynamic, dynamic>(),
    );
  }

  @override
  void write(BinaryWriter writer, Convo obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.userIds)
      ..writeByte(2)
      ..write(obj.lastMessage);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ConvoAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
