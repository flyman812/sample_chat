import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sample_chat/models/user/user.dart';
import 'package:sample_chat/screens/messaging/newMessageScreen.dart';
import 'package:sample_chat/services/database.dart';

class NewMessageProvider extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return StreamProvider<List<UserModel>>.value(
      value: Database.streamUsers(),
      initialData: [],
      child: NewMessageScreen(),
    );
  }
}
