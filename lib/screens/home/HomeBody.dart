import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sample_chat/models/convo/convo.dart';
import 'package:sample_chat/models/user/user.dart';
import 'package:sample_chat/res/fonts.dart';
import 'package:sample_chat/screens/messaging/widgets/convoWidget.dart';
import 'package:sample_chat/utils/enums.dart';

class HomeBody extends StatelessWidget {
  final User firebaseUser;
  final List<Convo> convos;
  final List<UserModel> users;
  HomeBody({
    Key? key,
    required this.firebaseUser,
    required this.convos,
    required this.users,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    NetworkStatus networkStatus = Provider.of<NetworkStatus>(context);
    return Column(
      children: [
        Visibility(
          visible: networkStatus == NetworkStatus.Offline,
          child: Container(
              width: MediaQuery.of(context).size.width,
              height: 40.0,
              alignment: Alignment.center,
              color: Colors.black,
              child: Text(
                "لطفا اتصال خود به اینترنت را چک کنید",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 12.0,
                  fontFamily: Fonts.iranYekan,
                ),
              )),
        ),
        Expanded(
          child: ListView(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              physics: BouncingScrollPhysics(
                  parent: AlwaysScrollableScrollPhysics()),
              children: getWidgets(context, firebaseUser, convos, users)),
        ),
      ],
    );
  }

  List<Widget> getWidgets(BuildContext? context, User? user,
      List<Convo>? _convos, List<UserModel>? _users) {
    final List<Widget> list = <Widget>[];
    if (_convos != null && _users != null && user != null) {
      final Map<String, UserModel> userMap = getUserMap(_users);
      for (Convo c in _convos) {
        if (c.userIds![0] == user.uid) {
          list.add(ConvoListItem(
              user: user,
              peer: userMap[c.userIds![1]],
              lastMessage: c.lastMessage!));
        } else {
          list.add(ConvoListItem(
              user: user,
              peer: userMap[c.userIds![0]],
              lastMessage: c.lastMessage!));
        }
      }
    }

    return list;
  }

  Map<String, UserModel> getUserMap(List<UserModel> users) {
    final Map<String, UserModel> userMap = Map();
    for (UserModel u in users) {
      userMap[u.id!] = u;
    }
    return userMap;
  }
}
