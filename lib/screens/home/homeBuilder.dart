import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sample_chat/models/convo/convo.dart';
import 'package:sample_chat/models/user/user.dart';
import 'package:sample_chat/providers/newMessageProvider.dart';
import 'package:sample_chat/res/fonts.dart';
import 'package:sample_chat/screens/home/HomeBody.dart';
import 'package:sample_chat/screens/messaging/widgets/convoWidget.dart';
import 'package:sample_chat/services/Authentication.dart';
import 'package:sample_chat/services/NetworkStatusService.dart';
import 'package:sample_chat/utils/enums.dart';

class HomeBuilder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final User firebaseUser = Provider.of<User>(context);
    final List<Convo> _convos = Provider.of<List<Convo>>(context);
    final List<UserModel> _users = Provider.of<List<UserModel>>(context);
    return Scaffold(
      appBar: AppBar(
          title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          IconButton(
              onPressed: Authentication.handleLogout,
              icon: RotationTransition(
                turns: AlwaysStoppedAnimation(180/360),
                child: Icon(Icons.logout, size: 30))),
          Text(firebaseUser.displayName!, style: TextStyle(fontSize: 18)),
          IconButton(
              onPressed: () => createNewConvo(context),
              icon: Icon(Icons.add, size: 30))
        ],
      )),
      body: StreamProvider<NetworkStatus>(
        initialData: NetworkStatus.Online,
        create: (context) =>
            NetworkStatusService().networkStatusController.stream,
        child: HomeBody(
          convos: _convos,
          firebaseUser: firebaseUser,
          users: _users,
        ),
      )
    );
  }

  void createNewConvo(BuildContext context) {
    Navigator.of(context).push<dynamic>(MaterialPageRoute<dynamic>(
        builder: (BuildContext context) => NewMessageProvider()));
  }

  Map<String, UserModel> getUserMap(List<UserModel> users) {
    final Map<String, UserModel> userMap = Map();
    for (UserModel u in users) {
      userMap[u.id!] = u;
    }
    return userMap;
  }

  List<Widget> getWidgets(
      BuildContext? context, User? user, List<Convo>? _convos, List<UserModel>? _users) {
    final List<Widget> list = <Widget>[];
    if (_convos != null && _users != null && user != null) {
      final Map<String, UserModel> userMap = getUserMap(_users);
      for (Convo c in _convos) {
        if (c.userIds![0] == user.uid) {
          list.add(ConvoListItem(
              user: user,
              peer: userMap[c.userIds![1]],
              lastMessage: c.lastMessage!));
        } else {
          list.add(ConvoListItem(
              user: user,
              peer: userMap[c.userIds![0]],
              lastMessage: c.lastMessage!));
        }
      }
    }

    return list;
  }
}
