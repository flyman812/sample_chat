import 'package:flutter/material.dart';
import 'package:sample_chat/models/user/user.dart';
import 'package:sample_chat/res/assets.dart';
import 'package:sample_chat/res/sizing.dart';
import 'package:sample_chat/screens/messaging/newConversationScreen.dart';
import 'package:sample_chat/utils/helperFunctions.dart';

class UserRow extends StatelessWidget {
  const UserRow({@required this.uid, @required this.contact});
  final String? uid;
  final UserModel? contact;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => createConversation(context),
      child: Container(
          margin: EdgeInsets.all(10.0),
          padding: EdgeInsets.all(10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: 50,
                height: 50,
                margin: EdgeInsets.symmetric(horizontal:10.0),
                decoration: BoxDecoration(shape: BoxShape.circle),
                child: ClipRRect(
                  borderRadius: Sizing.radius50,
                  child: contact!.photoUrl != null
                      ? FadeInImage(
                          placeholder: AssetImage(Assets.profileImage),
                          image: NetworkImage(contact!.photoUrl!),
                          fit: BoxFit.cover,
                        )
                      : Image.asset(
                          Assets.profileImage,
                          fit: BoxFit.cover,
                        ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(contact!.name!,
                      style:
                          TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold)),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical:10.0),
                    child: Text(contact!.email!,style: TextStyle(
                      fontSize: 12.0,
                      
                    ),),
                  )
                ],
              ),
            ],
          )),
    );
  }

  void createConversation(BuildContext context) {
    String convoID = HelperFunctions.getConvoID(uid!, contact!.id!);
    Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (BuildContext context) => NewConversationScreen(
            uid: uid!, contact: contact, convoID: convoID)));
  }
}
