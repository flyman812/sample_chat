import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:intl/intl.dart';
import 'package:sample_chat/models/user/user.dart';
import 'package:sample_chat/res/assets.dart';
import 'package:sample_chat/res/fonts.dart';
import 'package:sample_chat/res/sizing.dart';
import 'package:sample_chat/screens/messaging/newConversationScreen.dart';

class ConvoListItem extends StatelessWidget {
  ConvoListItem(
      {Key? key,
      @required this.user,
      @required this.peer,
      @required this.lastMessage})
      : super(key: key);

  final User? user;
  final UserModel? peer;
  Map<dynamic, dynamic>? lastMessage;

  BuildContext? context;
  String? groupId;
  bool? read;

  @override
  Widget build(BuildContext context) {
    if (lastMessage!['idFrom'] == user!.uid) {
      read = true;
    } else {
      read = lastMessage!['read'] == null ? true : lastMessage!['read'];
    }
    this.context = context;
    groupId = getGroupChatId();

    return Container(
      margin:
          const EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 5.0),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            buildContent(context),
          ],
        ),
      ),
    );
  }

  Widget buildContent(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => NewConversationScreen(
                uid: user!.uid, contact: peer, convoID: getGroupChatId())));
      },
      child: Padding(
        padding: const EdgeInsets.only(bottom: 10.0),
        child: Row(children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 5.0),
            child: Column(
              children: <Widget>[
                Container(
                    width: MediaQuery.of(context).size.width * 0.85,
                    child: buildConvoDetails(
                      context: context,
                      title: peer!.name!,
                      imageUrl: peer!.photoUrl!,
                    )),
              ],
            ),
          ),
        ], crossAxisAlignment: CrossAxisAlignment.start),
      ),
    );
  }

  Widget buildConvoDetails({
    required BuildContext context,
    required String title,
    required String imageUrl,
  }) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 70.0,
              height: 70.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
              ),
              child: Stack(
                fit: StackFit.loose,
                children: [
                  Container(
                    width: 60,
                    height: 60,
                    child: ClipRRect(
                      borderRadius: Sizing.radius50,
                      child: imageUrl != null
                          ? FadeInImage(
                              placeholder: AssetImage(Assets.profileImage),
                              image: NetworkImage(imageUrl),
                              fit: BoxFit.cover,
                            )
                          : Image.asset(
                              Assets.profileImage,
                              fit: BoxFit.cover,
                            ),
                    ),
                  ),
                  Visibility(
                    visible: read!,
                    child: Positioned(
                      bottom: 10.0,
                      right: 10.0,
                      child: Icon(
                        Icons.brightness_1,
                        color: Colors.greenAccent,
                        size: 18,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    textAlign: TextAlign.left,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontFamily: Fonts.iranYekan,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10.0),
                    child: Text(
                      lastMessage!['content'],
                      textAlign: TextAlign.left,
                      maxLines: 1,
                      overflow: TextOverflow.clip,
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontFamily: Fonts.iranYekan,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Spacer(),
            Text(
              getTime(lastMessage!['timestamp']),
              textAlign: TextAlign.right,
              style: TextStyle(
                fontWeight: FontWeight.w300,
                fontFamily: Fonts.iranYekan,
              ),
            )
          ],
        ),
      ],
    );
  }

  String getTime(String timestamp) {
    DateTime dateTime =
        DateTime.fromMillisecondsSinceEpoch(int.parse(timestamp));
    DateFormat format;
    if (dateTime.difference(DateTime.now()).inMilliseconds <= 86400000) {
      format = DateFormat('jm');
    } else {
      format = DateFormat.yMd('en_US');
    }
    return format
        .format(DateTime.fromMillisecondsSinceEpoch(int.parse(timestamp)));
  }

  String getGroupChatId() {
    if (user!.uid.hashCode <= peer!.id.hashCode) {
      return user!.uid + '_' + peer!.id!;
    } else {
      return peer!.id! + '_' + user!.uid;
    }
  }
}
