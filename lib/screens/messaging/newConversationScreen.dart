import 'package:bubble/bubble.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:sample_chat/models/user/user.dart';
import 'package:sample_chat/res/assets.dart';
import 'package:sample_chat/res/fonts.dart';
import 'package:sample_chat/res/sizing.dart';
import 'package:sample_chat/services/database.dart';

class NewConversationScreen extends StatelessWidget {
  const NewConversationScreen(
      {@required this.uid, @required this.contact, @required this.convoID});
  final String? uid, convoID;
  final UserModel? contact;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size(width, 50.0),
          child: AppBar(
              automaticallyImplyLeading: true,
              title: Row(
                children: [
                  Container(
                    width: 35.0,
                    height: 35.0,
                    margin:EdgeInsets.only(right: 10.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                    ),
                    child: ClipRRect(
                      borderRadius: Sizing.radius50,
                      child: contact!.photoUrl != null
                          ? FadeInImage(
                              placeholder: AssetImage(Assets.profileImage),
                              image: NetworkImage(contact!.photoUrl!),
                              fit: BoxFit.cover,
                            )
                          : Image.asset(
                              Assets.profileImage,
                              fit: BoxFit.cover,
                            ),
                    ),
                  ),
                  Text(contact!.name!),
                ],
              )),
        ),
        body: ChatScreen(uid: uid!, convoID: convoID!, contact: contact));
  }
}

class ChatScreen extends StatefulWidget {
  const ChatScreen(
      {@required this.uid, @required this.convoID, @required this.contact});
  final String? uid, convoID;
  final UserModel? contact;

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  String? uid, convoID;
  UserModel? contact;
  List<DocumentSnapshot>? listMessage;

  final TextEditingController textEditingController = TextEditingController();
  final ScrollController listScrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    uid = widget.uid;
    convoID = widget.convoID;
    contact = widget.contact;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus!.unfocus();
      },
      child: Container(
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                buildMessages(),
                buildInput(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buildInput() {
    return Container(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            textDirection: TextDirection.rtl,
            children: <Widget>[
              // Edit text
              Flexible(
                child: Container(
                  child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: TextField(
                        autofocus: true,
                        maxLines: 5,
                        controller: textEditingController,
                        textAlign: TextAlign.start,
                        textDirection: TextDirection.rtl,
                        decoration: const InputDecoration.collapsed(
                          hintStyle: TextStyle(
                            fontFamily: Fonts.iranYekan,
                          ),
                          hintText: '...برای دوستت یه چیزی بنویس',
                        ),
                      )),
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 8.0),
                child: IconButton(
                  icon: RotationTransition(
                      turns: AlwaysStoppedAnimation(180 / 360),
                      child: Icon(Icons.send, size: 25)),
                  onPressed: () => onSendMessage(textEditingController.text),
                ),
              ),
            ],
          ),
        ),
        width: double.infinity,
        height: 100.0);
  }

  Widget buildMessages() {
    return Flexible(
      child: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('messages')
            .doc(convoID)
            .collection(convoID!)
            .orderBy('timestamp', descending: true)
            .limit(20)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            listMessage = snapshot.data!.docs;
            return ListView.builder(
              padding: const EdgeInsets.all(10.0),
              itemBuilder: (BuildContext context, int index) =>
                  buildItem(index, snapshot.data!.docs[index]),
              itemCount: snapshot.data!.docs.length,
              reverse: true,
              controller: listScrollController,
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }

  Widget buildItem(int index, DocumentSnapshot document) {
    if (!document['read'] && document['idTo'] == uid) {
      Database.updateMessageRead(document, convoID!);
    }

    if (document['idFrom'] == uid) {
      // Right (my message)
      return Row(
        children: <Widget>[
          // Text
          Container(
              margin: EdgeInsets.symmetric(vertical: 5),
              child: Bubble(
                color: Colors.blueGrey,
                elevation: 0,
                padding: const BubbleEdges.all(10.0),
                nip: BubbleNip.rightTop,
                child: Text(
                  document['content'],
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: Fonts.iranYekan,
                  ),
                ),
              ),
              width: 200)
        ],
        mainAxisAlignment: MainAxisAlignment.end,
      );
    } else {
      // Left (peer message)
      return Container(
        margin: EdgeInsets.symmetric(vertical: 5),
        child: Column(
          children: <Widget>[
            Row(children: <Widget>[
              Container(
                child: Bubble(
                  color: Colors.black87,
                  elevation: 0,
                  padding: const BubbleEdges.all(10.0),
                  nip: BubbleNip.leftTop,
                  child: Text(
                    document['content'],
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: Fonts.iranYekan,
                    ),
                  ),
                ),
                width: 200.0,
                margin: const EdgeInsets.only(left: 10.0),
              )
            ])
          ],
          crossAxisAlignment: CrossAxisAlignment.start,
        ),
      );
    }
  }

  void onSendMessage(String content) {
    if (content.trim() != '') {
      textEditingController.clear();
      content = content.trim();
      Database.sendMessage(convoID!, uid!, contact!.id!, content,
          DateTime.now().millisecondsSinceEpoch.toString());
      listScrollController.animateTo(0.0,
          duration: Duration(milliseconds: 300), curve: Curves.easeOut);
    }
  }
}
