import 'package:flutter/material.dart';
import 'package:sample_chat/services/Authentication.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Login')),
      body: Container(
          child: Center(
        child: ElevatedButton(
          child: Text('Sign in with Google'),
          onPressed: Authentication.handleLogin,
        ),
      )),
    );
  }
}
